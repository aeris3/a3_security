<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Robert Ferencek <rferencek@aeris3.si>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


$GLOBALS['LANG']->includeLLFile('EXT:a3_security/mod1/locallang.xml');
//require_once(PATH_t3lib . 'class.t3lib_scbase.php');
$GLOBALS['BE_USER']->modAccess($MCONF, 1);	// This checks permissions and exits if the users has no permission for entry.

// DEFAULT initialization of a module [END]


/**
 * Module 'TYPO3 Security' for the 'a3_security' extension.
 *
 * @author	Robert Ferencek <rferencek@aeris3.si>
 * @package	TYPO3
 * @subpackage	tx_a3security
 */
class tx_a3security_module1 extends t3lib_SCbase {
	protected $pageinfo;
    private $destFolder = 'typo3temp/a3_security/';
    private $permissionsFile='ENABLE_PERMISSIONS';

	/**
	 * Initializes the module.
	 *
	 * @return void
	 */
	public function init() {
		parent::init();
	}

	/**
	 * Adds items to the ->MOD_MENU array. Used for the function menu selector.
	 *
	 * @return	void
	 */
	public function menuConfig() {
		$this->MOD_MENU = array(
			'function' => array(
				'1' => $GLOBALS['LANG']->getLL('main'),
				'2' => $GLOBALS['LANG']->getLL('about'),
			)
		);
		parent::menuConfig();
	}

	/**
	 * Main function of the module. Write the content to $this->content
	 * If you chose "web" as main module, you will need to consider the $this->id parameter which will contain the uid-number of the page clicked in the page tree
	 *
	 * @return void
	 */
	public function main() {
		// Access check!
		// The page will show only if there is a valid page and if this page may be viewed by the user
		$this->pageinfo = t3lib_BEfunc::readPageAccess($this->id, $this->perms_clause);
		$access = is_array($this->pageinfo) ? 1 : 0;
	
		if (($this->id && $access) || ($GLOBALS['BE_USER']->user['admin'] && !$this->id)) {

			// Draw the header.
			$this->doc = t3lib_div::makeInstance('mediumDoc');
			$this->doc->backPath = $GLOBALS['BACK_PATH'];
			$this->doc->form = '<form action="" method="post" enctype="multipart/form-data">';

			// JavaScript
			$this->doc->JScode = '
				<script language="javascript" type="text/javascript">
					script_ended = 0;
					function jumpToUrl(URL)	{
						document.location = URL;
					}
				</script>
			';
            
			$this->doc->postCode = '
				<script language="javascript" type="text/javascript">
					script_ended = 1;
					if (top.fsMod) top.fsMod.recentIds["web"] = 0;
				</script>
			';
            
			$this->content .= $this->doc->startPage($GLOBALS['LANG']->getLL('title'));
			$this->content .= $this->doc->header($GLOBALS['LANG']->getLL('title'));
			$this->content .= $this->doc->spacer(5);
			$this->content .= $this->doc->section('',$this->doc->funcMenu($headerSection, t3lib_BEfunc::getFuncMenu($this->id, 'SET[function]', $this->MOD_SETTINGS['function'], $this->MOD_MENU['function'])));
			$this->content .= $this->doc->divider(5);

			// Render content:
			$this->moduleContent();

			$this->content .= $this->doc->spacer(10);
		} else {
            
			// If no access or if ID == zero
			$this->doc = t3lib_div::makeInstance('mediumDoc');
			$this->doc->backPath = $GLOBALS['BACK_PATH'];

			$this->content .= $this->doc->startPage($GLOBALS['LANG']->getLL('title'));
			$this->content .= $this->doc->header($GLOBALS['LANG']->getLL('title'));
			$this->content .= $this->doc->spacer(5);
			$this->content .= $this->doc->spacer(10);
		}
	
	}

	/**
	 * Prints out the module HTML.
	 *
	 * @return void
	 */
	public function printContent() {
		$this->content .= $this->doc->endPage();
		echo '<div style="margin-left:15px; margin-top:10px;">'.$this->content.'</div>';
	}

	/**
	 * Generates the module content.
	 *
	 * @return void
	 */
	protected function moduleContent() {
        
        $features = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['a3_security']);
        
        $disableProtectionKey=true;
        
        if($features['enableKeyProtection']==1)
            $disableProtectionKey=false;
        
        //whole path with filename
        $perrmissionsPath = PATH_site.$this->destFolder.$this->permissionsFile;
        
		switch ((string)$this->MOD_SETTINGS['function']) {
			case 1:
				
                if($_POST['enablePermissions']=="1") {
                    if($_POST['key']==$features['protectionKey'] || $features['enableKeyProtection']!=1) {
                        $ourFileHandle = fopen($perrmissionsPath, 'w') or die("can't open file");
                        fwrite($ourFileHandle, "0\n");
                        fclose($ourFileHandle);
                    }
                    else
                        $error="background-color:#ffd8d8;";
                }
                
                $permissionStatus='<span style="color:green;">'.$GLOBALS['LANG']->getLL('disabled').'</span>';
                
                if(file_exists($perrmissionsPath)) {
                    
                    if(trim(file_get_contents($perrmissionsPath))=="1") {
                        $permissionStatus='
                            <span style="color:red;">'.$GLOBALS['LANG']->getLL('enabled').'</span> 
                            <small>('.date("d.m.Y ".$GLOBALS['LANG']->getLL('at')." H:i", filemtime($perrmissionsPath)).")</small>";
                        $disableProtectionKey=true;
                    }
                    else {
                        $permissionStatus='<span style="color:blue;">'.$GLOBALS['LANG']->getLL('pending').'</span>';
                        $pendingCounter='
                            <div style="float:left; margin-top:0px;"><img src="/typo3conf/ext/a3_security/res/preloader.gif" /></div>
                            <script type="text/javascript">
                                var timer=10;
                                function countdown() {
                                    setTimeout(function() {
                                        timer--;
                                        countdown();
                                    
                                        if(timer<1)
                                            window.location.reload();
                                
                                    }, 1000);
                                }
                                
                                countdown();
                            </script>
                        ';
                        $disableProtectionKey=true;
                    }
                    
                    $disableInput='disabled="disabled"';
                }
                
                $content.='
                    <table style="width:100%; font-size:12px;" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>'.$GLOBALS['LANG']->getLL('permissionStatus').'</td>
                            <td>'.$permissionStatus.'</td>
                            <td>'.$pendingCounter.'</td>
                        </tr>
                        
                        <tr>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                        ';
                if(!$disableProtectionKey) {
                    $content.='
                        <tr>
                            <td>'.$GLOBALS['LANG']->getLL('protection_key').'</td>
                            <td colspan="2">
                                <input style="'.$error.'" type="password" value="" name="key" />
                            </td>
                        </tr>
                        <tr><td colspan="3">&nbsp;</td></tr>';
                }
                
                $content.='
                        <tr>
                            <td>'.$GLOBALS['LANG']->getLL('action').'</td>
                            <td colspan="2">
                                <input type="submit" value="'.$GLOBALS['LANG']->getLL('enablePermissions').'" '.$disableInput.'/>
                                <input type="hidden" name="enablePermissions" value="1" />
                            </td>
                        </tr>
                    </table>
                ';
               
                
                $this->content.=$content;
				break;
			case 2:
				$this->content .= '<br />
                    <h2>'.$GLOBALS['LANG']->getLL('company').'</h2>
                    '.$GLOBALS['LANG']->getLL('companyAddress').'<br />
                    '.$GLOBALS['LANG']->getLL('companyPost').'<br />
                    '.$GLOBALS['LANG']->getLL('companyWeb').'<br />
                    '.$GLOBALS['LANG']->getLL('companyMail').'<br />
                ';
				break;
		}
	}
}



if (defined('TYPO3_MODE') && isset($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/a3_security/mod1/index.php'])) {
	include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/a3_security/mod1/index.php']);
}


// Make instance:
/** @var $SOBE tx_a3security_module1 */
$SOBE = t3lib_div::makeInstance('tx_a3security_module1');
$SOBE->init();

// Include files?
foreach ($SOBE->include_once as $INC_FILE) {
	include_once($INC_FILE);
}

$SOBE->main();
$SOBE->printContent();

?>