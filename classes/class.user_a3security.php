<?php
class user_a3security implements tx_em_renderHook {
    
    private $destFolder = 'typo3temp/a3_security/';
    private $permissionsFile='ENABLE_PERMISSIONS';
   

    public function render($pageRenderer, &$settings, &$content) {
        $path = t3lib_extMgm::extRelPath('a3_security');
        
        $perrmissionsPath = PATH_site.$this->destFolder.$this->permissionsFile;
        if(!file_exists($perrmissionsPath))
            $pageRenderer->addJsFile($path . 'res/check_write_permissions.js');
    }
}
?>
