<?php

########################################################################
# Extension Manager/Repository config file for ext "a3_security".
#
# Auto generated 23-08-2012 16:08
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'TYPO3 security',
	'description' => 'Extension enables/disables apache write permission on typo3conf/ext, typo3conf/localconf.php, typo3conf/extTables.php and other user defined files or folders.',
	'category' => 'module',
	'author' => 'Robert Ferencek',
	'author_email' => 'rferencek@aeris3.si',
	'shy' => '',
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => 'typo3temp/a3_security',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'Aeris3 d.o.o.',
	'version' => '1.2.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '4.5-0.0.0',
			'php' => '5.3-0.0.0',
			'cms' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:18:{s:9:"ChangeLog";s:4:"de85";s:21:"ext_conf_template.txt";s:4:"4514";s:12:"ext_icon.gif";s:4:"12dd";s:17:"ext_localconf.php";s:4:"c9c7";s:14:"ext_tables.php";s:4:"35ff";s:10:"README.txt";s:4:"e0e6";s:33:"classes/class.user_a3security.php";s:4:"7523";s:14:"doc/manual.sxw";s:4:"0338";s:19:"doc/wizard_form.dat";s:4:"fa66";s:20:"doc/wizard_form.html";s:4:"d3ff";s:13:"mod1/conf.php";s:4:"ea77";s:14:"mod1/index.php";s:4:"8d2d";s:18:"mod1/locallang.xml";s:4:"20da";s:22:"mod1/locallang_mod.xml";s:4:"7105";s:19:"mod1/moduleicon.png";s:4:"e0fe";s:30:"res/check_write_permissions.js";s:4:"a3ed";s:17:"res/preloader.gif";s:4:"ba06";s:39:"res/typo3_permission_handler.sh.example";s:4:"d983";}',
	'suggests' => array(
	),
);

?>